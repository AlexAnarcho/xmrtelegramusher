import 'dotenv/config'
import QrCode from 'qrcode'

export default class InviteInvoice {
  public requesterUserId: number
  public isPayed: boolean
  public inviteLink: string
  public hasJoined: boolean
  public xmrAmount: number
  public txUri: string
  public subaddress: string
  public subaddressIndex: number
  public qrCode: string = ''

  readonly joinedChatId = process.env.JOIN_GROUP_ID

  constructor({
    requesterUserId = 0,
    subaddress = '',
    subaddressIndex = 0,
    inviteLink = '',
    txUri = '',
  }) {
    this.requesterUserId = requesterUserId
    this.txUri = txUri
    this.subaddress = subaddress
    this.subaddressIndex = subaddressIndex
    this.inviteLink = inviteLink
    this.xmrAmount = Number(process.env.ENTRY_FEE_XMR)
    this.isPayed = false
    this.hasJoined = false
  }

  public setPayed(status = true) {
    this.isPayed = status
  }

  public setJoined(status = true) {
    this.hasJoined = status
  }

  public async generateQrCode() {
    const filePath = `data/${this.requesterUserId}.png`
    await QrCode.toFile(filePath, this.txUri, {
      version: 8,
    })
    this.qrCode = filePath
  }
}

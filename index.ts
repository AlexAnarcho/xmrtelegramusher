//@ts-nocheck
import { Telegraf } from 'telegraf'
import InvoiceManager from './InvoiceManager'
import 'dotenv/config'

const bot = new Telegraf(process.env.BOT_TOKEN)
const Invoices = new InvoiceManager()

function invoiceMessage({ isPayed, inviteLink, xmrAmount, subaddress }) {
  if (isPayed) return `**Thank you**🙏, you can join the group with this [link](${inviteLink}) 🥳🎉`
  else
    return `**🎫Outstanding Invoice**\n\nPlease pay *${xmrAmount} XMR* to *${subaddress}*\n\n⬇️ You can use the QRCode below:\n\nCheck your status with \`/check\``
}

bot.command('quit', (ctx) => {
  // Explicit usage
  ctx.telegram.leaveChat(ctx.message.chat.id)

  // Using context shortcut
  ctx.leaveChat()
})

bot.command('check', async (ctx) => {
  const { id: fromUserId } = ctx.update.message.from
  const userInvoice = await Invoices.checkInvoice({ fromUserId })
  if (userInvoice === undefined) ctx.replyWithMarkdown('Create an invoice with `/join`')
  const filePath = `data/${fromUserId}.png`

  const { isPayed, inviteLink, xmrAmount, subaddress } = userInvoice
  const message = invoiceMessage({ isPayed, inviteLink, xmrAmount, subaddress })
  ctx.replyWithMarkdown(message)
  ctx.replyWithPhoto({ source: filePath, filename: 'Your Invoice' })
})

bot.command('join', async (ctx) => {
  const { id: fromUserId } = ctx.update.message.from

  const existingInvoice = await Invoices.checkInvoice({ fromUserId })
  if (existingInvoice !== undefined) {
    ctx.reply('Please pay your outstanding invoice')
    return
  }

  const { telegram } = ctx
  const { invite_link: inviteLink } = await telegram.createChatInviteLink(
    process.env.JOIN_GROUP_ID,
    {
      member_limit: 1,
    },
  )

  await Invoices.createInvoice({ fromUserId, inviteLink })
  // 2. make a note of the invoice and the requester
  // 3. transfer the invoice to the requester
})

bot.launch()

// Enable graceful stop
process.once('SIGINT', () => bot.stop('SIGINT'))
process.once('SIGTERM', () => bot.stop('SIGTERM'))

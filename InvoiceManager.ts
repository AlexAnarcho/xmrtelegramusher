import 'dotenv/config'
//@ts-ignore
import * as monerojs from 'monero-javascript'
import InviteInvoice from './InviteInvoice'
import JSONdb from 'simple-json-db'

type TxUriProps = {
  address: string
  amount?: number
  description?: string
}
export default class InvoiceManager {
  wallet: monerojs.MoneroWalletRpc
  db: any
  readonly joinedChatId = process.env.JOIN_GROUP_ID
  readonly rpcAddress = process.env.RPC_ADDRESS
  readonly rpcUser = process.env.RPC_USER
  readonly rpcPassword = process.env.RPC_PASSWORD
  readonly walletName = process.env.WALLET_NAME
  readonly walletPassword = process.env.WALLET_PASSWORD

  constructor() {
    this.db = new JSONdb(process.env.DB_LOCATION || './data/db.json')
    this.getWallet()
  }

  private createMoneroTransactionUri({ address, amount, description }: TxUriProps) {
    return `monero:${address}?tx_amount=${amount}&tx_description=${description}`
  }

  private async getWallet() {
    const walletRpc = await monerojs.connectToWalletRpc(
      this.rpcAddress,
      this.rpcUser,
      this.rpcPassword,
    )
    const userWalletConfig = { path: this.walletName, password: this.walletPassword }
    this.wallet = await walletRpc.openWallet(userWalletConfig)
  }

  public async createInvoice({
    fromUserId,
    inviteLink,
  }: {
    fromUserId: number
    inviteLink: string
  }) {
    const label = String(fromUserId)
    const { state } = await this.wallet.createSubaddress(0, label)
    const { address, index } = state
    const txUri = this.createMoneroTransactionUri({
      address,
      amount: Number(process.env.ENTRY_FEE_XMR),
      description: label,
    })
    const newInvoice = new InviteInvoice({
      requesterUserId: fromUserId,
      subaddress: address,
      subaddressIndex: index,
      inviteLink,
      txUri,
    })

    await newInvoice.generateQrCode()

    this.db.set(String(newInvoice.requesterUserId), newInvoice)
  }

  public async checkInvoice({ fromUserId }: { fromUserId: number }) {
    const dbKey = String(fromUserId)
    const dbObj = this.db.get(dbKey)
    if (dbObj === undefined) {
      console.log('No invoice found')
      return undefined
    }

    const userInvoice: InviteInvoice = dbObj as InviteInvoice

    const { subaddressIndex, xmrAmount } = userInvoice
    if (!this.wallet) this.getWallet()
    const { state } = await this.wallet.getSubaddress(0, subaddressIndex)
    const { balance } = state

    if (xmrAmount <= monerojs.MoneroUtils.atomicUnitsToXmr(balance)) {
      userInvoice.isPayed = true
      this.db.set(dbKey, userInvoice)
    }

    return userInvoice
  }
}
